﻿
local RADAR_CENTER_X = UI.PrimaryMonitorWidth/2
local RADAR_CENTER_Y = UI.PrimaryMonitorHeight/2
local RADAR_RADIUS = 500
local RADAR_COLOR = '#00000000'
local RADAR_BORDER_WIDTH = 1
local RADAR_BORDER_COLOR = '#60FFFFFF'

local CENTER_POINT_SIZE = 4
local CENTER_POINT_COLOR = '#88FFFFFF'

local MAX_NUMBER_OF_TARGETS = 128

local TARGET_POINT_SIZE = 5
local TARGET_POINT_COLOR_DEFAULT = '#FFFF4500'
local TARGET_POINT_COLOR_MONSTER = '#FFFF0000'
local TARGET_POINT_COLOR_PLAYER = '#FF00FF00'
local TARGET_POINT_COLOR_PARTY = '#FF2196F3'
local TARGET_POINT_COLOR_RAID = '#FF2196F3'

local TARGET_LINE_WIDTH = 1

local TARGET_Z_SCALE = 0.5

local DEFAULT_RADAR_SCALE = 10

local OFFSET_ELEVATION = 0

local ROTATE_ELEVATION = true

local DRAW_TARGET_POINT_ON_PLANE = false

local LIMIT_ROTATE_ELEVATION_180 = true

local ALPHA_ON_FLIP_SIDE = 128 -- 0-255


RADAR = {}

Attunement.AddOnLoad(function()
  RADAR.circle = Attunement.Ellipse:New{
    CenterX = RADAR_CENTER_X,
    CenterY = RADAR_CENTER_Y,
    RadiusX = RADAR_RADIUS,
    RadiusY = RADAR_RADIUS,
    Hidden = true,
    Color = RADAR_COLOR,
    BorderWidth = RADAR_BORDER_WIDTH,
    BorderColor = RADAR_BORDER_COLOR,
    Layer = 'Low',
  }
  RADAR.center = Attunement.Rectangle:New{
    X = RADAR_CENTER_X-CENTER_POINT_SIZE/2,
    Y = RADAR_CENTER_Y-CENTER_POINT_SIZE/2,
    Width = CENTER_POINT_SIZE,
    Height = CENTER_POINT_SIZE,
    Color = CENTER_POINT_COLOR,
    Hidden = true,
    Layer = 'High',
  }
  RADAR.direction = Attunement.Texture:New{
    X = RADAR_CENTER_X-72,
    Y = RADAR_CENTER_Y-72,
    Width = 144,
    Height = 144,
    ScaleCenterX = RADAR_CENTER_X,
    ScaleCenterY = RADAR_CENTER_Y,
    FilePath = 'player_direction.png',
    Hidden = true,
    Layer = 'High',
  }
  RADAR.targetPoints = {}
  RADAR.targetLines = {}
  for i=1, MAX_NUMBER_OF_TARGETS do
    RADAR.targetPoints[i] = Attunement.Rectangle:New{
      Width = TARGET_POINT_SIZE,
      Height = TARGET_POINT_SIZE,
      Color = TARGET_POINT_COLOR_DEFAULT,
      Hidden = true,
      Layer = 'Medium',
    }
    RADAR.targetLines[i] = Attunement.Rectangle:New{
      Width = TARGET_LINE_WIDTH,
      Color = TARGET_POINT_COLOR_DEFAULT,
      Hidden = true,
      Layer = 'Medium',
    }
  end
end)

Attunement.AddOnFrame(function(ticks)
  for i=1, MAX_NUMBER_OF_TARGETS do
    RADAR.targetPoints[i].Hidden = true
    RADAR.targetLines[i].Hidden = true
  end

  if not FF.HasPlayer() then
    RADAR.circle.Hidden = true
    RADAR.center.Hidden = true
    RADAR.direction.Hidden = true
  else
    local playerID = FF.GetPlayer().ID

    local party = FF.GetParty()
    local partyMembers = nil
    local raidMembers = nil
    if party.IsInParty then
      partyMembers = party.PartyMembers
    end
    if party.IsInRaid then
      raidMembers = party.RaidMembers
    end

    RADAR.circle.Hidden = false
    RADAR.center.Hidden = false
    RADAR.direction.Hidden = false

    local cameraInfo = FF.GetCameraInfo()
    local diffX = cameraInfo.CameraX - cameraInfo.FocusX
    local diffY = cameraInfo.CameraY - cameraInfo.FocusY
    local diffZ = cameraInfo.CameraZ - cameraInfo.FocusZ
    local diffXY = math.sqrt(diffX^2 + diffY^2)
    local azimuthRadian = math.atan2(diffX, diffY)
    local elevationRadian
    if ROTATE_ELEVATION then
      elevationRadian = math.atan2(diffXY, diffZ)
    else
      elevationRadian = math.pi/2
    end
    elevationRadian = elevationRadian + OFFSET_ELEVATION
    if LIMIT_ROTATE_ELEVATION_180 then
      if elevationRadian > math.pi then
        elevationRadian = math.pi
      end
      if elevationRadian < 0 then
        elevationRadian = 0
      end
    end
    local scaleY = math.cos(elevationRadian)
    local scaleZ = -math.sin(elevationRadian)

    RADAR.circle.RadiusY = RADAR_RADIUS * scaleY
    RADAR.direction.ScaleY = scaleY

    local playerX = FF.GetPlayer().PositionX
    local playerY = FF.GetPlayer().PositionY
    local playerZ = FF.GetPlayer().PositionZ
    local originX = RADAR.circle.CenterX
    local originY = RADAR.circle.CenterY

    local radarScale = math.max(DEFAULT_RADAR_SCALE,0.01)
    local drawDistance = RADAR_RADIUS^2

    local mapScale = EORZEA_MAP.GetScaleByZoneName(FF.GetZoneName())
    if not mapScale then
      mapScale = 200
    end

    local combatants = FF.GetAllCombatants()
    local combatantsLength = math.min(combatants.Length, MAX_NUMBER_OF_TARGETS)

    for i=1, combatantsLength do
      local combatant = combatants[i-1]
      if combatant.ID ~= playerID then
        local combatantX = (combatant.PositionX - playerX) * mapScale/200 * radarScale
        local combatantY = (combatant.PositionY - playerY) * mapScale/200 * radarScale
        if combatantX^2 + combatantY^2 < drawDistance then
          local combatantZ = (combatant.PositionZ - playerZ) * mapScale/200 * radarScale

          local rotateCombatantX, rotateCombatantY = rotateXY(combatantX,combatantY,azimuthRadian)
          local pointX = rotateCombatantX + originX
          local pointY = rotateCombatantY * scaleY + originY
          RADAR.targetPoints[i].X = pointX - RADAR.targetPoints[i].Width/2
          if DRAW_TARGET_POINT_ON_PLANE then
            RADAR.targetPoints[i].Y = pointY - RADAR.targetPoints[i].Height/2
          else
            RADAR.targetPoints[i].Y = pointY + combatantZ*scaleZ*TARGET_Z_SCALE - RADAR.targetPoints[i].Height/2
          end
          RADAR.targetLines[i].X = pointX
          RADAR.targetLines[i].Y = pointY
          RADAR.targetLines[i].Height = combatantZ*scaleZ*TARGET_Z_SCALE

          if combatant.CombatantType == 'Player' then
            RADAR.targetPoints[i].Color = TARGET_POINT_COLOR_PLAYER
            RADAR.targetLines[i].Color = TARGET_POINT_COLOR_PLAYER
            if raidMembers then
              for j=0, raidMembers.Length-1 do
                if combatant.ID == raidMembers[j].ID then
                  RADAR.targetPoints[i].Color = TARGET_POINT_COLOR_RAID
                  RADAR.targetLines[i].Color = TARGET_POINT_COLOR_RAID
                end
              end
            end
            if partyMembers then
              for j=0, partyMembers.Length-1 do
                if combatant.ID == partyMembers[j].ID then
                  RADAR.targetPoints[i].Color = TARGET_POINT_COLOR_PARTY
                  RADAR.targetLines[i].Color = TARGET_POINT_COLOR_PARTY
                end
              end
            end
          elseif combatant.CombatantType == 'Monster' then
            RADAR.targetPoints[i].Color = TARGET_POINT_COLOR_MONSTER
            RADAR.targetLines[i].Color = TARGET_POINT_COLOR_MONSTER
          end

          if elevationRadian < math.pi/2 then
            if combatantZ < -0.1 then
              RADAR.targetPoints[i].Color.Alpha = ALPHA_ON_FLIP_SIDE
              RADAR.targetLines[i].Color.Alpha = ALPHA_ON_FLIP_SIDE
            end
          else
            if combatantZ > -0.1 then
              RADAR.targetPoints[i].Color.Alpha = ALPHA_ON_FLIP_SIDE
              RADAR.targetLines[i].Color.Alpha = ALPHA_ON_FLIP_SIDE
            end
          end

          RADAR.targetPoints[i].Hidden = false
          RADAR.targetLines[i].Hidden = false
        end
      end
    end
  end
end)

function rotateXY(x, y, radian)
  local retX = x*math.cos(radian) - y*math.sin(radian)
  local retY = x*math.sin(radian) + y*math.cos(radian)
  return retX, retY
end
