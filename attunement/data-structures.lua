
-- Ensure Attunement is defined
if not Attunement then
	Attunement = {}
end
-- Define Attunement is defined
Attunement.DataStructures = {}
local DS = Attunement.DataStructures


--[[
FIFOQueue
--]]

DS.FIFOQueue = {}
function DS.FIFOQueue:New()
	local self = {}

	-- Private variables
	local queue = {first = 0, last = -1}

	-- Public functions
	function self:Push( value )
		queue.last = queue.last + 1
		queue[queue.last] = value
	end

	function self:Pop()
		if queue.first > queue.last then
			return nil
		end

		local val = queue[queue.first]
		-- Setting to nil removes the key/value pair
		queue[queue.first] = nil
		queue.first = queue.first + 1
		return val
	end

	function self:Iter()
		local where = queue.first
		local var = 0
		return function ()
			if where <= queue.last then
				var = queue[where]
				where = where + 1
				return var
			end
		end
	end

	function self:Empty()
		return queue.first > queue.last
	end

	--[[
	Works like table.concat() mostly useful
	when the FIFO or upper classes are storing
	things that make sense to stringify.
	It will concat (join) the rangle of items
	starting with number first and ending with
	number last into a string inserting sep inbetween
	each item in the resulting string.
	--]]
	function self:Concat(sep, first, last)
		-- Character to use between each item
		local sep = sep or " "
		-- Index of first item to join
		local first = first or 1
		-- Index of last item to join
		local last = last or self:Count()
		-- As the queue gets used the first item
		-- internally will no longer be 1, but
		-- we hid that fact from the user of the
		-- class. thus we have to normalize the
		-- requested indexes to internal forms.
		first = queue.first + first - 1
		last = queue.first + last - 1
		-- Safty range check, the range must be completly within
		-- the bounds of the table that has consecuative indexes.
		-- If not table.concat will error out.
		if first >= queue.first and last <= queue.last and last > first then
			return table.concat(queue, sep, first, last)
		end
		-- Throw our own error instead of letting table.concat do it.
		-- This is only going to start hapening when the user sets bad
		-- ranges.
		error("range out of range ")
	end

	function self:Count()
		return queue.last - queue.first + 1
	end

	return self
end

--[[
Ring (Queue)
A Ring queue is constructed with a fixed size.
Once it grows to that size it starts removing
the oldest item with each new Push() in order to stay
that size. This has the effect of the Ring storing
the size most recent items Pushed.
--]]

DS.Ring = {}
function DS.Ring:Bless(self, size)
	-- Remove Pop() from the public interface
	-- but keep it for our own use.
	local BasePop = self.Pop
	self.Pop = nil

	-- Overload Push()
	local BasePush = self.Push
	function self:Push(item)
		BasePush(self, item)
		if self:Count() > size then
			BasePop(self)
		end
	end

	-- Iter(), Empty() and Count() from
	-- the base can stay the same

	return self
end

function DS.Ring:New(size)
	assert(type(size)=="number", "size must be a number")
	assert(size%1 == 0, "size must be a whole number")
	assert(size>=1, "size must be at least 1")
	local q = DS.FIFOQueue:New()
	return DS.Ring:Bless(q, size)
end

--[[
PriorityQueue

Implementation based on that of multiple authors, including
devurandom (gist) and the DBM authors.
--]]

DS.PriorityQueue = {}
function DS.PriorityQueue:New()
	local self = {}

	-- Private
	local heap = {}
	local nextFree = 1

	-- Public
	function self:Push(value)
		-- TODO: DBM's scheduler uses cache tables for small stuff, may be worth looking
		--       into for performance?

		-- If there's no priority set, set it really low
		if not value.priority then
			value.priority = 0
		end

		-- Add value to the heap
		local position = nextFree
		heap[position] = value
		nextFree = nextFree + 1

		-- Move the item up
		local parent = math.floor(position / 2)
		while position > 1 and heap[parent].priority > heap[position].priority do
			heap[parent], heap[position] = heap[position], heap[parent]
			position = parent
			parent = math.floor(position / 2)
		end
	end

	function self:Pop()
		local toReturn = heap[1]

		nextFree = nextFree - 1
		heap[1], heap[nextFree] = heap[nextFree], nil

		-- Move the item down again
		local position = 1
		local child
		while 2 * position < nextFree do
			child = 2 * position
			if child + 1 ~= nextFree and heap[child].priority >= heap[child + 1].priority then
				child = child + 1
			end

			-- We've done our work here
			if heap[position].priority <= heap[child].priority then break end

			heap[child], heap[position] = heap[position], heap[child]
			position = child
		end

		return toReturn
	end

	function self:Peek()
		return heap[1]
	end

	function self:Empty()
		return self:Peek() == nil
	end

	function self:Clear()
		heap = {}
		nextFree = 1
	end

	return self
end
